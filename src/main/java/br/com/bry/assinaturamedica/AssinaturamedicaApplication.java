package br.com.bry.assinaturamedica;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AssinaturamedicaApplication {

	public static void main(String[] args) throws IOException {
		adicionaMetadados();
	}
	
	private static void adicionaMetadados() throws IOException {
		// Token de autenticação gerado no BRy Cloud
		String token = "tokenDeAutenticação";
		//	Documento que será assinado
		File arquivo = new File("arquivos/teste.pdf");
		
		RequestSpecification form = RestAssured.given()
				.header("Authorization", token)
				.header("Content-Type", "multipart/form-data")
			.multiPart("prescricao[0][documento]", arquivo)
			// Tipo de prescrição. [Valores disponíveis: MEDICAMENTO, ATESTADO, SOLICITACAO_EXAME,
			// LAUDO, SUMARIA_ALTA, ATENDIMENTO_CLINICO, DISPENSACAO_MEDICAMENTO, VACINACAO e 
			// RELATORIO_MEDICO].
			.multiPart("prescricao[0][tipo]", "MEDICAMENTO")
			// Tipo de profissional. [Valores disponíveis: MEDICO ou FARMACEUTICO]
			.multiPart("profissional", "MEDICO")
			// Número de registro do profissional
			.multiPart("numeroRegistro", "123456")
			// Sigla do estado em que o número de registro do profissional está cadastrado.
			.multiPart("UF", "SC")
			// Especialidade do profissional
			.multiPart("especialidade", "ONCOLOGISTA");
		
		Response response = null;
		
		try {
			response = form.post("https://hub2.bry.com.br/pdf/v1/prescricao");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// O retorno será o arquivo com os MetaDados relacionados à prescrição, que agora precisará ser assinado.
		FileOutputStream arquivoComMetadados = new FileOutputStream("arquivos/arquivoComMetadados.pdf");
		arquivoComMetadados.write(response.body().asByteArray());
		

	}

}
